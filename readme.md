
# README #


### What is this repository for? ###

* Frontend Test: 3pc
* Version 0.1.0

### How do I get set up? ###

* git clone https://joerdis78@bitbucket.org/joerdis78/3pc-test.git
* Dependencies: node.js, gulp
* install gulp dependencies in src-root: npm install
* to compile css-files run "gulp sass" in root path

### Who do I talk to? ###

* Joerdis Schuller: joerdis.schuller@t-online.de
