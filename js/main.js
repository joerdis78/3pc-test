jQuery(document).ready(function () {
    jQuery('.btn--category').click(function () {
        jQuery('.btn.active').removeClass('active');
        jQuery(this).addClass('active');
        var contentData = jQuery(this).attr('data-class');
        jQuery('.content').hide();
        jQuery('.content.content--' + contentData).fadeIn();
        jQuery('.overlay').addClass('responsive-show');
    });
    jQuery('.close').click(function () {
        jQuery('.overlay').css('display', 'none');
    });
    jQuery('.close-box').click(function () {
        jQuery('.overlay').removeClass('responsive-show');
    });
    jQuery('.btn--start').click(function () {
        jQuery('.overlay').css('display', 'flex');
    });
});